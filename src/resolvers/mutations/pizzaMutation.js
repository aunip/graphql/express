const pizzaModel = require('../../models/pizzaModel');
const { bsonToJson } = require('../../utils');

const createPizza = (_, args) => {
  return new Promise((resolve, reject) => {
    pizzaModel.create(args, (err, result) => {
      err ? reject(err) : resolve({ createdId: bsonToJson(result).id });
    });
  });
};

const updatePizza = (_, args) => {
  const { id, ...arg } = args;

  return new Promise((resolve, reject) => {
    pizzaModel.findByIdAndUpdate(id, arg, (err, result) => {
      err ? reject(err) : resolve({ updatedId: bsonToJson(result).id });
    });
  });
};

const deletePizza = (_, { id }) => {
  return new Promise((resolve, reject) => {
    pizzaModel.findByIdAndDelete(id, (err, result) => {
      err ? reject(err) : resolve({ deletedId: bsonToJson(result).id });
    });
  });
};

module.exports = {
  createPizza,
  updatePizza,
  deletePizza
};
