const { connect, clear, disconnect } = require('../../mongodMock');
const { createPizza, updatePizza, deletePizza } = require('./pizzaMutation');

describe('pizzaMutation', () => {
  beforeAll(async () => await connect());

  afterEach(async () => await clear());

  afterAll(async () => await disconnect());

  it('createPizza', async () => {
    const { createdId } = await createPizza(undefined, {
      label: 'PizzaTella',
      items: ['Choco', 'Nuts'],
      price: 9.99
    });

    expect(createdId).toBeTruthy();
  });

  it('updatePizza', async () => {
    const { createdId } = await createPizza(undefined, {
      label: 'PizzaTella',
      items: ['Choco', 'Nuts'],
      price: 9.99
    });

    const { updatedId } = await updatePizza(undefined, {
      id: createdId,
      items: ['Nutella'],
      price: 10
    });

    expect(updatedId).toEqual(createdId);
  });

  it('deletePizza', async () => {
    const { createdId } = await createPizza(undefined, {
      label: 'PizzaTella',
      items: ['Choco', 'Nuts'],
      price: 9.99
    });

    const { deletedId } = await deletePizza(undefined, {
      id: createdId
    });

    expect(deletedId).toEqual(createdId);
  });
});
