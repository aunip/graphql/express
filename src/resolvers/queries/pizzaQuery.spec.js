const { connect, clear, disconnect } = require('../../mongodMock');
const { createPizza } = require('../mutations/pizzaMutation');
const { pizzas, pizza } = require('./pizzaQuery');

describe('pizzaMutation', () => {
  beforeAll(async () => await connect());

  afterEach(async () => await clear());

  afterAll(async () => await disconnect());

  it('pizzas', async () => {
    for (let i = 1; i <= 10; i++) {
      await createPizza(undefined, {
        label: `Test ${i}`,
        items: [`${i}`],
        price: i
      });
    }

    const results = await pizzas();

    expect(results).toHaveLength(10);
  });

  it('pizza', async () => {
    const { createdId } = await createPizza(undefined, {
      label: 'PizzaTella',
      items: ['Choco', 'Nuts'],
      price: 9.99
    });

    const result = await pizza(undefined, { id: createdId });

    expect(result.id).toEqual(createdId);
    expect(result.label).toEqual('PizzaTella');
    expect(result.items).toHaveLength(2);
    expect(result.price).toEqual(9.99);
  });
});
