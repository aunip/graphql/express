const pizzaModel = require('../../models/pizzaModel');
const { bsonToJson } = require('../../utils');

const pizzas = (_, args) => {
  return new Promise((resolve, reject) => {
    pizzaModel.find(args, (err, results) => {
      err ? reject(err) : resolve(results.map(result => bsonToJson(result)));
    });
  });
};

const pizza = (_, { id }) => {
  return new Promise((resolve, reject) => {
    pizzaModel.findById(id, (err, result) => {
      err ? reject(err) : resolve(bsonToJson(result));
    });
  });
};

module.exports = {
  pizzas,
  pizza
};
