const pizzaQueries = require('./queries/pizzaQuery');
const pizzaMutations = require('./mutations/pizzaMutation');

const resolvers = {
  Query: {
    ...pizzaQueries
  },
  Mutation: {
    ...pizzaMutations
  }
};

module.exports = resolvers;
