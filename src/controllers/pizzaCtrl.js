const pizzaModel = require('../models/pizzaModel');
const { isFileExist, readJsonFile, isDefined, bsonToJson } = require('../utils');

/**
 * Initialize Pizzas
 * Read JSON & Feed DB
 */
const initPizzas = () => {
  if (isFileExist('pizzas.json')) {
    const pizzas = readJsonFile('pizzas.json');

    pizzas.forEach(pizza => {
      const { label, items, price } = pizza;

      pizzaModel
        .findOne({ label })
        .then(result => {
          if (!result) {
            pizzaModel.create(pizza).catch(err => {
              console.log(err);
            });
          } else {
            result.items = items;
            result.price = price;

            result.save(err => {
              if (isDefined(err)) {
                console.log(err);
              }
            });
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  }
};

module.exports = {
  initPizzas
};
