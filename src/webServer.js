const express = require('express');
const expressGraphQl = require('express-graphql');
const expressPlayground = require('graphql-playground-middleware-express').default;
const cors = require('cors');
const { json } = require('body-parser');
const { makeExecutableSchema } = require('graphql-tools');
const resolvers = require('./resolvers');
const { getSchema } = require('./utils');

/**
 * Web Server
 */
function webServer() {
  const server = express();
  const port = 5050;

  applyMiddleware();
  mountRoutes();

  /**
   * Apply Middleware
   * CORS / JSON / URL Encode
   */
  function applyMiddleware() {
    server.use(cors());
    server.use(json());
  }

  /**
   * Mount Routes
   * Pizza(s) API
   */
  function mountRoutes() {
    server.use(
      '/graphql',
      expressGraphQl({
        graphiql: true,
        schema: makeExecutableSchema({
          typeDefs: getSchema('schema.graphqls'),
          resolvers
        })
      })
    );

    server.get('/playground', expressPlayground({ endpoint: '/graphql' }));
  }

  /**
   * Launch Server
   * Display...
   */
  function launchServer() {
    server.listen(port, () => {
      console.log(`> Listening On 'http://localhost:${port}'`);
    });
  }

  return {
    launchServer
  };
}

module.exports = webServer;
