const { Schema, model } = require('mongoose');

/**
 * Pizza Model
 *
 * @param {String} label Label
 * @param {Array} items Items
 * @param {Number} price Price
 */
const pizzaSchema = new Schema(
  {
    label: {
      type: String,
      unique: true
    },
    items: {
      type: [String]
    },
    price: {
      type: Number,
      min: 1,
      max: 99
    }
  },
  {
    versionKey: false
  }
);

module.exports = model('pizzas', pizzaSchema);
