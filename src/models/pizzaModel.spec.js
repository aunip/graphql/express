const { connect, clear, disconnect } = require('../mongodMock');
const pizzaModel = require('./pizzaModel');

describe('pizzaModel', () => {
  beforeAll(async () => await connect());

  afterEach(async () => await clear());

  afterAll(async () => await disconnect());

  it('Should Be Created', async () => {
    const pizza = await pizzaModel.create({
      label: 'PizzaTella',
      items: ['Choco', 'Nuts'],
      price: 9.99
    });

    expect(pizza.label).toEqual('PizzaTella');
    expect(pizza.items).toHaveLength(2);
    expect(pizza.price).toEqual(9.99);
  });
});
