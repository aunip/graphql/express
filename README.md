# All U Need Is Pizza

> _Made With **NodeJS** 12_

Isomorphic Projects For Testing Technologies With CRUD Pattern

## File Structure

```
.
+-- src
    +-- controllers
        +-- pizzaCtrl.js
    +-- models
        +-- pizzaModel.js
        +-- pizzaModel.spec.js
    +-- resolvers
        +-- mutations
            +-- pizzaMutation.js
            +-- pizzaMutation.spec.js
        +-- queries
            +-- pizzaQuery.js
            +-- pizzaQuery.spec.js
        +-- index.js
    +-- utils
        +-- index.js
    +-- main.js
    +-- mongodMock.js
    +-- pizzas.json
    +-- schema.graphqls
    +-- webServer.js
+-- .gitignore
+-- jest.config.js
+-- LICENSE
+-- package.json
+-- README.md
+-- yarn.lock
```

## Process

Repository:

```
git clone https://gitlab.com/aunip/graphql/express.git
```

Install:

```
npm install
```

Launch:

```
npm run start
```

Test:

```
npm run test
```

### Requirement

- [x] **MongoDB** Server

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
